<?php
/**
 * User: krona
 * Date: 05.01.15
 * Time: 17:20
 */
return [
    'console' => [
        'router' => [
            'routes' => [
                'planner:run' => [
                    'options' => [
                        'route' => 'krona distribute',
                        'defaults' => [
                            'controller' => 'Krona\CommonModule\Controller\Console',
                            'action' => 'distribute'
                        ],
                    ]
                ],
            ],
        ],
    ],
    'krona' => [
        'types' => [
            'converters' => [
                'date' => \Krona\Common\Common\Converter\DatetimeConverter::class,
                'datetime' => \Krona\Common\Common\Converter\DatetimeConverter::class,
                'association' => \Krona\Common\Common\Converter\AssociationConverter::class,
                'array' => \Krona\Common\Common\Converter\ArrayConverter::class,
                'json_array' => \Krona\Common\Common\Converter\ArrayConverter::class,
                'integer' => \Krona\Common\Common\Converter\IntegerConverter::class,
            ],
        ],
        'reflection' => [
            'cache' => [
                'dir' => 'data/Krona/Reflection/Cache',
                'debug' => true,
            ],
        ],
        'mvc' => [
            'converters' => [
                'abstract_converters' => [
                    [
                        'priority' => -99,
                        'converter' => \Krona\CommonModule\Mvc\Param\ServiceParamConverter::class,
                    ],
                    [
                        'priority' => -100,
                        'converter' => \Krona\CommonModule\Mvc\Param\RouteParamConverter::class,
                    ],
                    [
                        'priority' => -101,
                        'converter' => \Krona\CommonModule\Mvc\Param\DataParamConverter::class,
                    ],
                ],
            ],
        ],
        'objects' => [
//            'Object\Name' => [
//                'dir' => 'module/Application/Object',
//                'name' => 'Name',
//                'namespace' => 'Object',
//                'uses' => [
//                    'Foo\Bar',
//                    'Baz' => 'Foo\Baz',
//                ],
//                'annotations' => [
//                    'Annotation' => '',
//                    'Target' => '{"CLASS"}'
//                ],
//                'extends' => 'Bar',
//                'traits' => [
//                    'Baz',
//                ],
//                'constants' => [
//                    'STATUS_ACTIVE' => 1,
//                ],
//                'properties' => [
//                    'id' => [
//                        'value' => 5, //May be added
//                        'annotations' => [
//                            'Identifier' => '',
//                        ]
//                    ],
//                ],
//                'methods' => [
//                    'getFoo' => [
//                        'body' => 'return $this->getId() . $value;',
//                        'annotations' => [
//                            //The same
//                        ],
//                    ],
//                    'parameters' => [
//                        [
//                            'name' => 'value',
//                            'value' => ' times',
//                        ]
//                    ],
//                ],
//            ],
        ],
    ],


    'controllers' => [
        'invokables' => [
            'Krona\CommonModule\Controller\Console' => 'Krona\CommonModule\Controller\ConsoleController',
        ],
    ],
    'service_manager' => [
        'invokables' => [
            \Krona\CommonModule\Mvc\Param\RouteParamConverter::class => \Krona\CommonModule\Mvc\Param\RouteParamConverter::class,
            \Krona\CommonModule\Mvc\Param\ServiceParamConverter::class => \Krona\CommonModule\Mvc\Param\ServiceParamConverter::class,
            \Krona\CommonModule\Mvc\Param\DataParamConverter::class => \Krona\CommonModule\Mvc\Param\DataParamConverter::class,
            \Krona\CommonModule\Reflection\Util\Reflector::class => \Krona\CommonModule\Reflection\Util\Reflector::class,
            \Krona\CommonModule\Mvc\Param\ParamManager::class => \Krona\CommonModule\Mvc\Param\ParamManager::class,
            \Krona\CommonModule\Service\InputFilterGenerator::class => \Krona\CommonModule\Service\InputFilterGenerator::class,
            \Krona\CommonModule\Service\Accessor\PropertyAccessor::class => \Krona\CommonModule\Service\Accessor\PropertyAccessor::class,
            \Krona\CommonModule\Service\ObjectGenerator::class => \Krona\CommonModule\Service\ObjectGenerator::class,
        ],
        'aliases' => [
            'krona.common.inputfilter.generator' => \Krona\CommonModule\Service\InputFilterGenerator::class,
        ],
    ],
];