<?php
/**
 * User: krona
 * Date: 05.01.15
 * Time: 17:22
 */

namespace Krona\CommonModule;


use Krona\CommonModule\Service\Accessor\PropertyAccessor;
use Krona\CommonModule\Service\Accessor\PropertyAccessorAwareInterface;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ServiceManager;

class Module
{
    protected $managers = [
        'FilterManager',
        'ControllerLoader',
        'ControllerPluginManager',
        'InputFilterManager',
        'ViewHelperManager',
        'ValidatorManager',
    ];

    public function onBootstrap(MvcEvent $event)
    {
        $sm = $event->getApplication()->getServiceManager();
        if ($sm instanceof ServiceManager) {
            $sm->addInitializer(function ($instance) use ($sm){
                if ($instance instanceof PropertyAccessorAwareInterface) {
                    /** @var PropertyAccessor $propertyAccessor */
                    $propertyAccessor = $sm->get(PropertyAccessor::class);
                    $propertyAccessor->process($instance);
                }
            });
            foreach($this->managers as $managerName) {
                /** @var AbstractPluginManager $manager */
                $manager = $sm->get($managerName);
                $manager->addInitializer(function ($instance) use ($sm){
                    if ($instance instanceof PropertyAccessorAwareInterface) {
                        /** @var PropertyAccessor $propertyAccessor */
                        $propertyAccessor = $sm->get(PropertyAccessor::class);
                        $propertyAccessor->process($instance);
                    }
                });
            }
        }
    }

    public function getConsoleUsage(Console $console){
        return array(
            'krona distribute' => 'Distribute Objects from config',
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/Resources/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }
}