<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 4:19 PM
 */

namespace Krona\CommonModule\Form\Type;


use Krona\Common\Common\Collection;
use Zend\InputFilter\Input;

class CollectionType extends Input
{
    /** @var  AbstractType */
    protected $type;

    protected $messages = [];

    /**
     * @param  mixed $context Extra "context" to provide the validator
     * @return bool
     */
    public function isValid($context = null)
    {
        // Empty value needs further validation if continueIfEmpty is set
        // so don't inject NotEmpty validator which would always
        // mark that as false
        if (!$this->continueIfEmpty()) {
            $this->injectNotEmptyValidator();
        }
        $validator = $this->getType();
        $value     = $this->getValue();
        $result = false;
        if ($this->isRequired() && !is_array($value)) {
            $result = $this->getValidatorChain()->isValid($value);
        } else {
            $messages = [];
            foreach($value as $key => $data) {
                $result    = $validator->isValid($data, $context);
                $messages[$key] = $validator->getMessages();
                if (!$result && $this->hasFallback()) {
                    $this->setValue($this->getFallbackValue());
                    $result = true;
                }
            }
            $this->messages = $messages;
        }

        return $result;
    }

    public function getMessages()
    {
        if (!empty($this->messages)) {
            return $this->messages;
        } else {
            return parent::getMessages();
        }
    }

    /**
     * @return AbstractType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param AbstractType $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}