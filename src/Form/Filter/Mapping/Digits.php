<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/11/14
 * Time: 11:07 AM
 */

namespace Krona\CommonModule\Form\Filter\Mapping;


use Doctrine\Common\Annotations\Annotation\Target;
use Zend\Filter\Digits as BaseDigits;
use Zend\Filter\FilterChain;

/**
 * Class Digits
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Digits extends BaseDigits
{

}