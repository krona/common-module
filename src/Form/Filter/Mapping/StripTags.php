<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:14
 */

namespace Krona\CommonModule\Form\Filter\Mapping;

use Krona\CommonModule\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\StripTags as BaseStripTags;

/**
 * Class StripTags
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class StripTags extends BaseStripTags
{

}