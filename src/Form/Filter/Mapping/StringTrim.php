<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 0:55
 */

namespace Krona\CommonModule\Form\Filter\Mapping;

use Zend\Filter\StringTrim as BaseStringTrim;

/**
 * Class StringTrim
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class StringTrim extends BaseStringTrim
{

}