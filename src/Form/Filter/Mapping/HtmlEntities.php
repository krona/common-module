<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:12
 */

namespace Krona\CommonModule\Form\Filter\Mapping;

use Zend\Filter\HtmlEntities as BaseHtmlEntities;

/**
 * Class HtmlEntities
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class HtmlEntities extends BaseHtmlEntities
{

}