<?php
/**
 * User: granted
 * Date: 1/29/15
 * Time: 8:40 PM
 */

namespace Krona\CommonModule\Service;


use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Krona\CommonModule\Service\Accessor\PropertyAccessorAwareInterface;
use Krona\CommonModule\Service\Generator\ObjectWriter;

class ObjectGenerator implements PropertyAccessorAwareInterface
{
    /**
     * @Service(name="config")
     * @var array
     */
    protected $config;

    public function distribute()
    {
        foreach($this->config['krona']['objects'] as $rawObject) {
            $this->generateObject($rawObject)->write();
        }
    }

    public function generateObject(array $config)
    {
        return new ObjectWriter($config);
    }
}