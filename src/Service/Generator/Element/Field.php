<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 25.08.14
 * Time: 4:14
 */

namespace Krona\CommonModule\Service\Generator\Element;


use Arilas\Proxy\Annotation\DoctrineAnnotation;
use Arilas\Proxy\Document\ClassDocument;
use Arilas\Proxy\Element\Method;
use Arilas\Proxy\Element\Property;

class Field
{
    /** @var  string */
    protected $name;
    /** @var ClassDocument */
    protected $document;

    public function __construct(ClassDocument $document, $name, array $config = [])
    {
        $this->document = $document;
        $this->name = $name;

        $this->parseConfig($config);
    }

    protected function parseConfig(array $config = [])
    {
        $property = new Property();
        $property->setName($this->name);

        if (isset($config['value'])) {
            $property->setValue($config['value']);
        }

        $this->document->addProperty($property);

        if (isset($config['annotations'])) {
            foreach ($config['annotations'] as $type => $value) {
                $annotation = new DoctrineAnnotation();
                $annotation->setType($type);
                $annotation->setValue($value);
                $property->addAnnotation($annotation);
            }
        }

        $this->addSetter();
        $this->addGetter();
    }

    protected function addSetter()
    {
        $method = new Method();
        $method->setName('set' . ucfirst($this->name));
        $method->setBody('$this->' . $this->name . ' = $' . $this->name . ';' . PHP_EOL . 'return $this;');
        $method->addParameter($this->name);
        $this->document->addMethod($method);
    }

    protected function addGetter()
    {
        $method = new Method();
        $method->setName('get' . ucfirst($this->name));
        $method->setBody('return $this->' . $this->name . ';');
        $this->document->addMethod($method);
    }
} 