<?php
/**
 * User: granted
 * Date: 1/29/15
 * Time: 8:42 PM
 */

namespace Krona\CommonModule\Service\Generator;


use Arilas\Proxy\Annotation\DoctrineAnnotation;
use Arilas\Proxy\Document\ClassDocument;
use Arilas\Proxy\Element\Method;
use Krona\CommonModule\Service\Generator\Element\Field;

class ObjectWriter
{
    /** @var  ClassDocument */
    protected $document;
    /** @var  string */
    protected $dir;
    /** @var  string */
    protected $name;
    /** @var  array */
    protected $config;

    public function __construct(array $config)
    {
        $this->document = new ClassDocument();
        $this->config = $config;
        $this->dir = $config['dir'];
        $this->parseConfig($config);
    }

    protected function parseConfig(array $config)
    {
        $this->name = $config['name'];
        $this->document->setName($config['name']);
        $this->document->setNamespace($config['namespace']);

        if (isset($config['uses'])) {
            foreach ($config['uses'] as $alias => $usage) {
                if (is_int($alias)) {
                    $this->document->addUseStatement($usage);
                } else {
                    $this->document->addUseStatement($usage, $alias);
                }
            }
        }

        if (isset($config['annotations'])) {
            foreach ($config['annotations'] as $type => $value) {
                $annotation = new DoctrineAnnotation();
                $annotation->setType($type);
                $annotation->setValue($value);
                $this->document->addClassAnnotation($annotation);
            }
        }

        if (isset($config['extends'])) {
            $this->document->setExtends($config['extends']);
        }

        if (isset($config['traits'])) {
            foreach ($config['traits'] as $mixin) {
                $this->document->addTrait($mixin);
            }
        }

        if (isset($config['constants'])) {
            foreach ($config['constants'] as $key => $value) {
                $this->document->addConstant($key, $value);
            }
        }

        if (isset($config['properties'])) {
            $this->processProperties($config['properties']);
        }

        if (isset($config['methods'])) {
            $this->processMethods($config['methods']);
        }
    }

    protected function processProperties(array $properties)
    {
        foreach ($properties as $name => $property) {
            new Field($this->document, $name, $property);
        }
    }

    protected function processMethods(array $methods)
    {
        foreach ($methods as $name => $rawMethod) {
            $method = new Method();
            $method->setName($name);
            $method->setBody($rawMethod['body']);

            if (isset($rawMethod['annotations'])) {
                foreach ($rawMethod['annotations'] as $type => $value) {
                    $annotation = new DoctrineAnnotation();
                    $annotation->setType($type);
                    $annotation->setValue($value);
                    $method->addAnnotation($annotation);
                }
            }

            if (isset($rawMethod['parameters'])) {
                foreach ($rawMethod['parameters'] as $parameter) {
                    $method->addParameter(
                        $parameter['name'],
                        (isset($parameter['value'])) ? $parameter['value'] : null,
                        (isset($parameter['type'])) ? $parameter['type'] : null
                    );
                }
            }

            $this->document->addMethod($method);
        }
    }

    public function write()
    {
        if (file_exists($this->dir) && is_writable($this->dir)) {
            file_put_contents(
                $this->dir . '/' . $this->name . '.php',
                $this->toString()
            );
        } else {
            throw new \Exception(
                $this->dir . ' must be writable'
            );
        }
    }

    public function execute()
    {
        $code = $this->document->toString();
        $code = str_replace('<?php', '', $code);
        eval($code);
    }

    public function toString()
    {
        return $this->document->toString();
    }
}