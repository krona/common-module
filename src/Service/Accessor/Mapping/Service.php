<?php
/**
 * User: granted
 * Date: 1/23/15
 * Time: 2:36 PM
 */

namespace Krona\CommonModule\Service\Accessor\Mapping;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Service
 * @package Krona\CommonModule\Service\Accessor\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Service
{
    public $name;
}