<?php
/**
 * User: granted
 * Date: 1/23/15
 * Time: 2:35 PM
 */

namespace Krona\CommonModule\Service\Accessor;


use Krona\CommonModule\Reflection\ReflectionProperty;
use Krona\CommonModule\Reflection\Util\Reflector;
use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class PropertyAccessor implements ServiceManagerAwareInterface
{
    /** @var  ServiceManager */
    protected $serviceManager;

    protected $instance;

    public function process($instance)
    {
        $this->instance = $instance;
        /** @var Reflector $reflector */
        $reflector = $this->serviceManager->get(Reflector::class);
        $class = $reflector->reflectClass($instance);
        foreach($class->getProperties() as $property) {
            $this->processProperty($property);
        }
    }

    protected function processProperty(ReflectionProperty $property)
    {
        $annotation = $property->getReader()->getPropertyAnnotation($property, Service::class);

        if ($annotation) {
            $this->injectValue($property, $annotation->name);
        }
    }

    protected function injectValue(ReflectionProperty $property, $service)
    {
        if ($this->serviceManager->has($service)) {
            $property->setAccessible(true);
            $property->setValue($this->instance, $this->serviceManager->get($service));
        }
    }

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
}