<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 8:00 PM
 */

namespace Krona\CommonModule\Mvc;


use Krona\CommonModule\Mvc\Exception\ParamNotFoundException;
use Krona\CommonModule\Mvc\Param\Annotation\NotFoundHandler;
use Krona\CommonModule\Mvc\Param\ParamManager;
use Zend\ServiceManager\ServiceManager;

/**
 * Class ActionExecutor
 * @package Krona\CommonModule\Mvc\Param
 * @method ServiceManager getServiceLocator()
 */
trait ActionExecutor
{
    public function execute($method)
    {
        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }
        /** @var ParamManager $paramManager */
        $paramManager = $this->getServiceLocator()->get(ParamManager::class);
        try {
            return $paramManager->executeAction($this, $method);
        } catch(ParamNotFoundException $e) {
            $annotations = $e->getMethod()->getAnnotations();
            $defaultAnnotation = null;
            foreach($annotations as $annotation) {
                if ($annotation instanceof NotFoundHandler) {
                    if (!is_null($annotation->parameter) && $annotation->parameter == $e->getParameter()->getName()) {
                        return $this->execute($annotation->method);
                    } elseif (is_null($annotation->parameter)) {
                        $defaultAnnotation = $annotation;
                    }
                }
            }
            if (!is_null($defaultAnnotation)) {
                return $this->execute($defaultAnnotation->method);
            }
            return $this->execute('notFoundAction');
        }
    }
}