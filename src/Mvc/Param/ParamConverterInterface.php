<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 7:44 PM
 */

namespace Krona\CommonModule\Mvc\Param;

use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

interface ParamConverterInterface extends ServiceLocatorAwareInterface
{
    public function canConvert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null);

    public function convert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null);
}