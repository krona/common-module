<?php
/**
 * User: granted
 * Date: 2/6/15
 * Time: 12:33 PM
 */

namespace Krona\CommonModule\Mvc\Param\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class NotFoundHandler
 * @package Krona\CommonModule\Mvc\Param\Annotation
 * @Annotation
 * @Target({"PROPERTY"})
 */
class NotFoundHandler
{
    public $parameter;
    public $method = 'notFoundAction';
}