<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 1:00
 */

namespace Krona\CommonModule\Mvc\Param\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class ServiceConverter
 * @package Krona\CommonModule\Mvc\Param\Annotation
 * @Annotation
 * @Target({"METHOD"})
 */
class ServiceConverter
{
    public $serviceName;

    public $parameter;
}