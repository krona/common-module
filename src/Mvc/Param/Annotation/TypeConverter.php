<?php
/**
 * User: granted
 * Date: 1/16/15
 * Time: 3:29 PM
 */

namespace Krona\CommonModule\Mvc\Param\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class TypeConverter
 * @package Krona\CommonModule\Mvc\Param\Annotation
 * @Annotation
 * @Target({"METHOD"})
 */
class TypeConverter
{
    public $parameter;

    public $targetClass;

    public $objectManager;
}