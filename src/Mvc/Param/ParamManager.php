<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 7:54 PM
 */

namespace Krona\CommonModule\Mvc\Param;

use Krona\CommonModule\Reflection\ReflectionMethod;
use Krona\CommonModule\Reflection\Util\Reflector;
use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class ParamManager implements ServiceLocatorAwareInterface
{
    /** @var  ServiceManager */
    protected $serviceManager;

    protected $config;

    protected $converters;

    public function executeAction(AbstractController $controller, $method)
    {
        /** @var Reflector $reflector */
        $reflector = $this->getServiceLocator()->get(Reflector::class);
        $parameters = [];
        $reflMethod = $reflector->reflectMethod($controller, $method);

        foreach($reflMethod->getParameters() as $parameter) {
            $value = $this->testAbstractConverters($parameter, $controller, $reflMethod);
            if (is_null($value)) {
                //TODO: Implement Converter by PhpDoc
            }
            $parameters[] = $value;
        }

        return call_user_func_array([$controller, $method], $parameters);
    }

    public function testAbstractConverters(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method)
    {
        $converters = $this->getConfig()['converters']['abstract_converters'];
        usort($converters, function($first, $second){
            return $first['priority'] < $second['priority'];
        });

        foreach ($converters as $converter) {
            $converter = $this->getConverter($converter);
            if ($converter->canConvert($parameter, $controller, $method)) {
                return $converter->convert($parameter, $controller, $method);
            }
        }

        return null;
    }

    /**
     * @param $converter
     * @return ParamConverterInterface
     */
    public function getConverter($converter)
    {
        if (!isset($this->getConverters()[$converter['converter']])) {
            $this->converters[$converter['converter']] = $this->getServiceLocator()->get($converter['converter']);
        }

        return $this->converters[$converter['converter']];
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        if (is_null($this->config)) {
            $this->config = $this->getServiceLocator()->get('Config')['krona']['mvc'];
        }
        return $this->config;
    }

    /**
     * @param mixed $config
     * @return $this
     */
    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConverters()
    {
        if (is_null($this->converters)) {
            $this->converters = [];
        }
        return $this->converters;
    }

    /**
     * @param mixed $converters
     * @return $this
     */
    public function setConverters($converters)
    {
        $this->converters = $converters;
        return $this;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceManager;
    }
}