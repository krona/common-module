<?php
/**
 * User: krona
 * Date: 15.01.15
 * Time: 23:58
 */

namespace Krona\CommonModule\Mvc\Param;

use Krona\CommonModule\Mvc\Exception\ParamNotFoundException;
use Krona\CommonModule\Mvc\Param\Annotation\ServiceConverter;
use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Mvc\Controller\AbstractController;

class ServiceParamConverter extends AbstractParamConverter
{
    public function canConvert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null)
    {
        if (!is_null($parameter->getClass())) {
            if($this->getServiceLocator()->has($parameter->getClass()->getName())){
                return true;
            }
        } else {
            /** @var ServiceConverter $annotation */
            $annotation = $method->getReader()->getMethodAnnotation($method, ServiceConverter::class);
            if (
                $annotation
                && $annotation->parameter == $parameter->getName()
                && $this->getServiceLocator()->has($annotation->serviceName)
            ) {
                return true;
            }
        }

        return false;
    }

    public function convert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null)
    {
        if (!is_null($parameter->getClass())) {
            if($this->getServiceLocator()->has($parameter->getClass()->getName())){
                return $this->getServiceLocator()->get($parameter->getClass()->getName());
            }
        } else {
            /** @var ServiceConverter $annotation */
            $annotation = $method->getReader()->getMethodAnnotation($method, ServiceConverter::class);
            if (
                $annotation
                && $annotation->parameter == $parameter->getName()
                && $this->getServiceLocator()->has($annotation->serviceName)
            ) {
                return $this->getServiceLocator()->get($annotation->serviceName);
            }
        }
        if ($parameter->allowsNull()) {
            return null;
        } else {
            throw new ParamNotFoundException($parameter, $method);
        }
    }
}