<?php
/**
 * User: krona
 * Date: 15.01.15
 * Time: 23:55
 */

namespace Krona\CommonModule\Mvc\Param;

use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Mvc\Controller\AbstractController;

class RouteParamConverter extends AbstractParamConverter
{
    public function canConvert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null)
    {
        return !!($controller->getEvent()->getRouteMatch()->getParam($parameter->getName(), false));
    }

    public function convert(\ReflectionParameter $parameter, AbstractController $controller, ReflectionMethod $method = null)
    {
        return $controller->getEvent()->getRouteMatch()->getParam($parameter->getName());
    }
}