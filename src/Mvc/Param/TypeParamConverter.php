<?php
/**
 * User: granted
 * Date: 1/16/15
 * Time: 3:27 PM
 */

namespace Krona\CommonModule\Mvc\Param;


use Krona\CommonModule\Form\Type\AbstractType;
use Krona\CommonModule\Mvc\Param\Annotation\TypeConverter;
use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Mvc\Controller\AbstractController;

abstract class TypeParamConverter extends AbstractParamConverter
{
    public function canConvert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        if (!$parameter->getClass() || $parameter->getClass()->isSubclassOf(AbstractType::class)) {
            return false;
        }

        $annotations = $method->getAnnotations();

        foreach($annotations as $annotation) {
            if ($annotation instanceof TypeConverter && $annotation->parameter == $parameter->getName()) {
                return $this->isManaged($parameter, $annotation);
            }
        }

        return false;
    }

    public function convert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        if (!$parameter->getClass() || $parameter->getClass()->isSubclassOf(AbstractType::class)) {
            return false;
        }

        $annotations = $method->getAnnotations();

        foreach($annotations as $annotation) {
            if ($annotation instanceof TypeConverter && $annotation->parameter == $parameter->getName() && $this->isManaged($parameter, $annotation)) {
                return $this->generateType($parameter, $controller, $method, $annotation);
            }
        }

        return null;
    }

    abstract protected function isManaged(
        \ReflectionParameter $parameter,
        TypeConverter $annotation
    );

    abstract protected function generateType(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method,
        TypeConverter $annotation
    );

    /**
     * Process the raw body content
     *
     * If the content-type indicates a JSON payload, the payload is immediately
     * decoded and the data returned. Otherwise, the data is passed to
     * parse_str(). If that function returns a single-member array with a key
     * of "0", the method assumes that we have non-urlencoded content and
     * returns the raw content; otherwise, the array created is returned.
     *
     * @param  mixed $request
     * @return object|string|array
     */
    protected function processBodyContent($request)
    {
        $content = $request->getContent();

        parse_str($content, $parsedParams);

        // If parse_str fails to decode, or we have a single element with key
        // 0, return the raw content.
        if (!is_array($parsedParams)
            || (1 == count($parsedParams) && isset($parsedParams[0]))
        ) {
            return $content;
        }

        return $parsedParams;
    }
}