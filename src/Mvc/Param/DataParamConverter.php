<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 2:19
 */

namespace Krona\CommonModule\Mvc\Param;

use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Http\Request as HttpRequest;
use Zend\Mvc\Controller\AbstractController;

class DataParamConverter extends AbstractParamConverter
{

    public function canConvert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        return $parameter->isArray() && $parameter->getName() == 'data' && $controller->getRequest() instanceof HttpRequest;
    }

    public function convert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        if (!$parameter->isArray() || $parameter->getName() != 'data' || !$controller->getRequest() instanceof HttpRequest) {
            return null;
        }

        if ($controller->getRequest()->isPatch() || $controller->getRequest()->isPut()) {
            return $this->processBodyContent($controller->getRequest());
        }

        return $controller->getRequest()->getPost()->toArray();
    }

    /**
     * Process the raw body content
     *
     * If the content-type indicates a JSON payload, the payload is immediately
     * decoded and the data returned. Otherwise, the data is passed to
     * parse_str(). If that function returns a single-member array with a key
     * of "0", the method assumes that we have non-urlencoded content and
     * returns the raw content; otherwise, the array created is returned.
     *
     * @param  mixed $request
     * @return object|string|array
     */
    protected function processBodyContent($request)
    {
        $content = $request->getContent();

        parse_str($content, $parsedParams);

        // If parse_str fails to decode, or we have a single element with key
        // 0, return the raw content.
        if (!is_array($parsedParams)
            || (1 == count($parsedParams) && isset($parsedParams[0]))
        ) {
            return $content;
        }

        return $parsedParams;
    }
}