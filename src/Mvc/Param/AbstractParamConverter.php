<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 7:47 PM
 */

namespace Krona\CommonModule\Mvc\Param;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Class AbstractParamConverter
 * @package Krona\CommonModule\Mvc\Param
 */
abstract class AbstractParamConverter implements ParamConverterInterface
{
    /** @var  ServiceManager */
    protected $serviceManager;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceManager;
    }
}