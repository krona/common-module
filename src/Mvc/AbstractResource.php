<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/23/14
 * Time: 4:51 PM
 */

namespace Krona\CommonModule\Mvc;


use Krona\CommonModule\Form\Type\AbstractType;
use Krona\CommonModule\Mvc\Exception\NotFoundException;
use Zend\Http\Request;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;

/**
 * Class AbstractResource
 * @package Arilas\ORM\Mvc\Controller
 * @method AbstractType createInputFilter($entity)
 * @method Request getRequest()
 */
class AbstractResource extends AbstractController
{
    use ActionExecutor;

    const ONE = 1;
    const MANY = 2;

    protected $actionMap = [
        Request::METHOD_GET => [
            self::ONE => 'get',
            self::MANY => 'getList',
        ],
        Request::METHOD_POST => [
            self::ONE => 'update', //for simplify update process
            self::MANY => 'create',
        ],
        Request::METHOD_PUT => [
            self::ONE => 'update',
            self::MANY => 'replaceList',
        ],
        Request::METHOD_PATCH => [
            self::ONE => 'patch',
            self::MANY => 'patchList',
        ],
        Request::METHOD_DELETE => [
            self::ONE => 'delete',
            self::MANY => 'deleteList',
        ],
    ];

    protected $overrideMap = [];

    protected $identifierName = 'id';

    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException(
                'Missing route matches; unsure how to retrieve action'
            );
        }

        $request = $this->getRequest();

        try {
            // Was an "action" requested?
            $action = $routeMatch->getParam('action', false);
            if ($action) {
                // Handle arbitrary methods, ending in Action
                $method = static::getMethodFromAction($action);
                if (!method_exists($this, $method)) {
                    $method = 'notFoundAction';
                }

                $return = $this->execute($method);
                $e->setResult($return);
                return $return;
            }

            // RESTful methods
            if (!empty($this->overrideMap)) {
                $this->actionMap = array_replace_recursive($this->actionMap, $this->overrideMap);
            }
            $action = $this->getAction($request->getMethod());
            $return = $this->execute($action);
            $routeMatch->setParam('action', $action);
            $e->setResult($return);
        } catch (NotFoundException $exception) {
            $response = $e->getResponse();
            $response->setStatusCode(405);
            return $response;
        }

        return $return;
    }

    protected function getAction($method)
    {
        if (!isset($this->actionMap[$method])) {
            throw new NotFoundException('Method doesn\'t mapped in this Resource');
        }
        if ($this->getIdentifier($this->getEvent()->getRouteMatch(), $this->getRequest())) {
            return $this->actionMap[$method][self::ONE];
        } else {
            return $this->actionMap[$method][self::MANY];
        }
    }

    /**
     * Retrieve the identifier, if any
     *
     * Attempts to see if an identifier was passed in either the URI or the
     * query string, returning it if found. Otherwise, returns a boolean false.
     *
     * @param  \Zend\Mvc\Router\RouteMatch $routeMatch
     * @param  Request $request
     * @return false|mixed
     */
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $id;
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $id;
        }

        return false;
    }

    /**
     * Retrieve the route match/query parameter name containing the identifier
     *
     * @return string
     */
    public function getIdentifierName()
    {
        return $this->identifierName;
    }
} 