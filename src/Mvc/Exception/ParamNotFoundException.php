<?php
/**
 * User: granted
 * Date: 2/6/15
 * Time: 12:18 PM
 */

namespace Krona\CommonModule\Mvc\Exception;


use Krona\CommonModule\Reflection\ReflectionMethod;

class ParamNotFoundException extends NotFoundException
{
    /** @var  \ReflectionParameter */
    protected $parameter;
    /** @var  ReflectionMethod */
    protected $method;

    public function __construct(\ReflectionParameter $parameter, ReflectionMethod $method)
    {
        $this->parameter = $parameter;
        $this->method = $method;
    }

    /**
     * @return \ReflectionParameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @return ReflectionMethod
     */
    public function getMethod()
    {
        return $this->method;
    }
}