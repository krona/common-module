<?php
/**
 * User: krona
 * Date: 10/21/14
 * Time: 4:42 PM
 */

namespace Krona\CommonModule\Mvc\Exception;


use Krona\Common\Exception\RuntimeException;

class NotFoundException extends RuntimeException
{

}