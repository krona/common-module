<?php
/**
 * User: granted
 * Date: 1/15/15
 * Time: 7:49 PM
 */

namespace Krona\CommonModule\Mvc;

use Krona\CommonModule\Mvc\ActionExecutor;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;

class AbstractController extends AbstractActionController
{
    use ActionExecutor;

    /**
     * Execute the request
     *
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException('Missing route matches; unsure how to retrieve action');
        }

        $action = $routeMatch->getParam('action', 'not-found');
        $method = static::getMethodFromAction($action);

        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }

        $actionResponse = $this->execute($method);

        $e->setResult($actionResponse);

        return $actionResponse;
    }
}