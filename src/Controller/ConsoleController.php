<?php
/**
 * User: granted
 * Date: 1/29/15
 * Time: 8:52 PM
 */

namespace Krona\CommonModule\Controller;


use Krona\CommonModule\Mvc\AbstractController;
use Krona\CommonModule\Service\ObjectGenerator;

class ConsoleController extends AbstractController
{
    public function distributeAction(ObjectGenerator $objectGenerator)
    {
        $objectGenerator->distribute();
    }
}