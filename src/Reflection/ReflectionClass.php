<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 0:06
 */

namespace Krona\CommonModule\Reflection;


use Doctrine\Common\Annotations\AnnotationReader;

class ReflectionClass extends \ReflectionClass
{
    protected $annotations;
    /** @var  AnnotationReader */
    protected $reader;
    /** @var  ReflectionMethod[] */
    protected $methods;
    /** @var  ReflectionProperty[] */
    protected $properties;

    /**
     * @param null $filter
     * @return array|ReflectionMethod[]
     */
    public function getMethods($filter = null)
    {
        if (is_null($this->methods)) {
            $rawMethods = parent::getMethods($filter);
            $this->methods = [];
            foreach($rawMethods as $rawMethod) {
                $method = new ReflectionMethod($rawMethod->getDeclaringClass(), $rawMethod->getName());
                $method->setReader($this->getReader());
                $this->methods[] = $method;
            }
        }

        return $this->methods;
    }

    public function getProperties($filter = null)
    {
        if (is_null($this->properties)) {
            $rawProperties = parent::getProperties();
            $this->properties = [];
            foreach($rawProperties as $rawProperty) {
                $property = new ReflectionProperty($this->name, $rawProperty->getName());
                $property->setReader($this->getReader());
                $this->properties[] = $property;
            }
        }

        return $this->properties;
    }

    /**
     * Return an annotations value
     * @return mixed
     */
    public function getAnnotations()
    {
        if (is_null($this->annotations)) {
            $this->annotations = $this->getReader()->getClassAnnotations($this);
        }
        return $this->annotations;
    }

    /**
     * Set annotations value
     * @param mixed $annotations
     * @return $this
     */
    public function setAnnotations($annotations)
    {
        $this->annotations = $annotations;
        return $this;
    }

    /**
     * Return an reader value
     * @return AnnotationReader
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * Set reader value
     * @param AnnotationReader $reader
     * @return $this
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
        return $this;
    }
}