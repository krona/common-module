<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 0:08
 */

namespace Krona\CommonModule\Reflection;


use Doctrine\Common\Annotations\AnnotationReader;

class ReflectionProperty extends \ReflectionProperty
{
    protected $annotations;
    /** @var  AnnotationReader */
    protected $reader;

    /**
     * Return an annotations value
     * @return mixed
     */
    public function getAnnotations()
    {
        if (is_null($this->annotations)) {
            $this->annotations = $this->getReader()->getPropertyAnnotations($this);
        }
        return $this->annotations;
    }

    /**
     * Set annotations value
     * @param mixed $annotations
     * @return $this
     */
    public function setAnnotations($annotations)
    {
        $this->annotations = $annotations;
        return $this;
    }

    /**
     * Return an reader value
     * @return AnnotationReader
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * Set reader value
     * @param AnnotationReader $reader
     * @return $this
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
        return $this;
    }
}