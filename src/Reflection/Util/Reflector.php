<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 0:18
 */

namespace Krona\CommonModule\Reflection\Util;


use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\FileCacheReader;
use Krona\CommonModule\Reflection\ReflectionClass;
use Krona\CommonModule\Reflection\ReflectionMethod;
use Krona\CommonModule\Reflection\ReflectionProperty;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class Reflector implements ServiceLocatorAwareInterface
{
    /** @var  ServiceManager */
    protected $serviceManager;

    protected $reader;

    public function reflectClass($class)
    {
        $reflection = new ReflectionClass($class);
        $reflection
            ->setReader($this->getReader())
        ;

        return $reflection;
    }

    public function reflectMethod($class, $method)
    {
        $reflection = new ReflectionMethod($class, $method);
        $reflection
            ->setReader($this->getReader())
        ;

        return $reflection;
    }

    public function reflectProperty($class, $property)
    {
        $reflection = new ReflectionProperty($class, $property);
        $reflection
            ->setReader($this->getReader())
        ;

        return $reflection;
    }

    /**
     * Return an reader value
     * @return mixed
     */
    public function getReader()
    {
        if (is_null($this->reader)) {
            $config = $this->getServiceLocator()->get('Config');
            $this->reader = new FileCacheReader(
                new AnnotationReader(),
                $config['krona']['reflection']['cache']['dir'],
                $config['krona']['reflection']['cache']['debug']
            );
        }
        return $this->reader;
    }

    /**
     * Set reader value
     * @param mixed $reader
     * @return $this
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
        return $this;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceManager;
    }
}